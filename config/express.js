var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var path = require('path');

module.exports = function(app, config) {
 
    // all environments
	app.set('port', process.env.PORT || process.env.VCAP_APP_PORT || 3000);
	app.set('views', './views');
	app.set('view engine', 'ejs');
	app.engine('html', require('ejs').renderFile);
	app.use(logger('dev'));
	app.use(bodyParser.urlencoded({
    	extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());
	app.use(express.static(path.join('./public')));
	app.use('/style', express.static(path.join('./views/style')));

	// development only
	if ('development' == app.get('env')) {
    	app.use(errorHandler());
	}
  
};