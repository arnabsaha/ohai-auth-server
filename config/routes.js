var rest = require('../server/controllers/rest');

module.exports = function(app){

  // find all songs route
  app.get('/location', rest.findAll);
  
  // find one song route
  app.get('/location/:id', rest.findOne);
  
  // Add song route
  app.post('/location', rest.add);
  
  // Update song route
  app.put('/location/:id', rest.update);
  
  // Delete song route
  app.delete('/location/:id', rest.remove);
};