var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Location = new Schema({
	uid: String,
	pushyId: String,
	location: { 'type': {type: String, enum: "Point", default: "Point"}, coordinates: { type: [Number],   default: [0,0]} }
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

Location.index({location: '2dsphere'});

mongoose.model('Location', Location);
