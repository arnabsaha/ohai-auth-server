var mongoose = require('mongoose'), Location = mongoose.model('Location');
var pushy = require('pushy-node');
var pushyAPI = new pushy('3209b49a76d4346df5cd693b22467179c92e2bb34a5f1dcb59280be34ae8440a');

var options = {
    // Set the notification to expire if not delivered within 30 seconds 
    time_to_live: 30
};

/**
** Location Model Exports
*/
exports.findAll = function(req, res) {
  Location.find({}, function(err, location) {
    if (err) {
      throw new Error(err);
    }
    res.send(location);
  });
};

exports.findOne = function(req, res) {
  Location.findById(req.params.id, function(err, location) {
    if (err) {
      throw new Error(err);
    }

    res.send(location);
  });
};


function findNearby(coordinates){
  console.log("coordinates: "+coordinates);

  Location.find({ location : { '$near' : {
                                  $geometry: {
                                    type:"Point", 
                                    coordinates: coordinates
                                  }, 
                                  $maxDistance:10000
                                }
                            } 

                }, function (err, location) {
                  console.log("size: "+location.length);
                
                if(location.length > 1){
                  var registrationIds = [];
                  var userIds = [];   
                  for (i = 0; i < location.length; i++) { 
                      console.log("nearby: "+location[i].pushyId);  
                      registrationIds.push(location[i].pushyId);
                      userIds.push(location[i].uid);                  
                    }

                  var message = {"message":{"action":"startdiscovery", "userIds": userIds}};
                  sendPush(message,registrationIds);
                }

              });

}


exports.add = function(req, res) {

  //console.log(req.body['uid']);

  Location.find( { uid: req.body['uid'] }, 
          function (err, location) {
            
            //UserId present in Mongo
            if(location.length == 1){

                console.log(location[0].id);
                Location.findByIdAndUpdate(location[0].id, {
                $set: req.body
                }, function(err, location) {
                  if (err) {
                    throw new Error(err);
                }
                res.send(location);
                });

            } 

            //New UserId introduced 
            else{

              var document = new Location(req.body);
              document.save(function(err, location) {
                if (err) {
                    throw new Error(err);
                }

                res.send(location);
              });
            }

          });

};

exports.update = function(req, res) {
  Location.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, function(err, location) {
    if (err) {
      throw new Error(err);
    }

    findNearby(location.location.coordinates);
    res.send(location);
  });
};


exports.remove= function(req, res) {
  Location.findByIdAndRemove(req.params.id, function(err, location) {
    if (err) {
      throw new Error(err);
    }
    res.send(location);
  });
};

function sendPush(data, registrationIds){
  console.log("Sending Push");
  pushyAPI.sendPushNotification(data, registrationIds, options, function (err, id) {
      // Log errors to console 
      if (err) {
          return console.log('Fatal Error', err);
      }
    
      // Log success 
      console.log('Push sent successfully! (ID: ' + id + ')');
  });
}