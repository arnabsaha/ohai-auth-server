'use strict';

require( 'dotenv' ).config( {silent: true} );

/**
 * Module dependencies.
 */

var express = require('express'), mongoose = require('mongoose'), fs = require('fs'),
    config = require('./config/config'), root = __dirname;

var app = express();

var modelsPath = __dirname + '/server/models';
fs.readdirSync(modelsPath).forEach(function (file) {
  if (file.indexOf('.js') >= 0) {
    require(modelsPath + '/' + file);
  }
});


require('./config/express')(app, config);
require('./config/routes')(app);


// // Imports from the boilerplate

// var fileToUpload;
// var multipart = require('connect-multiparty')
// var multipartMiddleware = multipart();


// Ohai server imports

var https = require('https')
var pushy = require('pushy-node');
var http = require('http').Server(app);
//var server = http.createServer(express);
var io =  require('socket.io')(http);
var pushyAPI = new pushy('3209b49a76d4346df5cd693b22467179c92e2bb34a5f1dcb59280be34ae8440a');
var Queue = require("firebase-queue");
var admin = require("firebase-admin");
var watson = require( 'watson-developer-cloud' );  // watson sdk


var serviceAccount = require("./ohai-beta-dev.json");

admin.initializeApp({
   credential: admin.credential.cert(serviceAccount),
   databaseURL: "https://ohai-beta-dev.firebaseio.com"
});

var ref = admin.database().ref('queue');
var queue = new Queue(ref, function (data, progress, resolve, reject) {
    processData(data);
    console.log(data);
    setTimeout(function () {
        resolve();
    }, 1000)

});

// Create the service wrapper
var conversation = watson.conversation( {
  url: 'https://gateway.watsonplatform.net/conversation/api',
  username: process.env.CONVERSATION_USERNAME || '<username>',
  password: process.env.CONVERSATION_PASSWORD || '<password>',
  version_date: '2016-07-11',
  version: 'v1'
} );



//LinkedIn Custom Firebase Auth

app.post("/sendauth", function(request, response) {
      
    // console.log(request.body.uid);  
    admin.auth().createCustomToken(request.body.uid)
    
    .then(function(customToken) {
        // Send token back to client
        response.send(customToken);
            response.end();
    })
    .catch(function(error) {
            console.log("Error creating custom token:", error);
    });
});

var placeType;
var cuisine;
var placeResults;
var nextPageToken;

/**
 * Updates the response text using the intent confidence
 * @param  {Object} res The node.js http response object
 * @param  {Object} input The request to the Conversation service
 * @param  {Object} response The response from the Conversation service
 * @return {Object}          The response with the updated message
 */
function updateMessage(res, input, response) {

 if ( !response.output ) {
    response.output = {};
  } else if ( checkAmenity( response ) ) {

    placeType = response.entities[0].value;
    
    return res.json(response);

  } if (checkCuisine (response ) ) {

    //console.log("cuisineRes"+ JSON.stringify(response));

    var placeSearch = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+
                        response.context.lat+','+response.context.long+'&rankby=distance&type='+placeType+'&keyword='+response.entities[0].value+'&key=AIzaSyAHpRHR_CN7OD6H7NKTAvf8ixqzo6mdyBs';

   // console.log(placeSearch);
   // console.log(response);

    https.get(placeSearch, function(resp){

        var body = '';
        
        resp.on('data', function(chunk){
            body += chunk;
        });

        resp.on('end', function(){
            var placeJSON = JSON.parse(body);
            var resultToPass = [];

            placeResults = placeJSON.results;
            nextPageToken = placeJSON.next_page_token;

           // console.log("initialResultSize: "+ placeResults.length);

            if(placeResults.length >= 5){
                for(var i = 0; i < 5; i ++){
                    //var result = placeResults[i];
                    //console.log(placeResults[i]);
                    resultToPass.push(placeResults[i]);
                    placeResults.splice(i, 1);
                }
            }
            
           // console.log("afterResultsSize: "+ placeResults.length);

            

            response.output = {};
            response.results = resultToPass;
            response.nextResults = placeResults.length;

           // console.log(response);

            //console.log("Result"+res.json(response));

            return res.json(response);
        });

    }).on("error", function(e){
        console.log("Got error: " + e.message);
    });

    

  }else if ( checkMore (response ) ) {
    //console.log(response);
    var resultToPass = [];

    if(placeResults.length >= 5){
        for(var i = 0; i < placeResults.length; i ++){
            //var result = placeResults[i];
            //console.log(placeResults[i]);
            resultToPass.push(placeResults[i]);
            placeResults.splice(i, 1);

            if(i == 4){
                break;
            }
        }
    } 

    if(placeResults.length < 5) {
        placeType = "";
        cuisine = "";
        placeResults = [];
        nextPageToken = "";
    }
                
    //console.log("afterResultsSize: "+ placeResults.length);

    response.output = {};
    response.results = resultToPass;
    response.nextResults = placeResults.length;

    //console.log(response);

    //console.log("Result"+res.json(response));

    return res.json(response);

  } else if ( checkNoMore (response ) ) {
    
    placeType = "";
    cuisine = "";
    placeResults = [];
    nextPageToken = "";
            
    console.log("noresultSize: "+ placeResults.length);

    response.output = {};
    response.results = resultToPass;
    response.nextResults = placeResults.length;

    //console.log("Result"+res.json(response));

    return res.json(response);

  }else if ( response.output && response.output.text ) {
    //console.log(response);
    return res.json( response );
  }
}

function checkAmenity(data) {
   // console.log("intentData", data);
  if(data.entities.length > 0){
    return data.intents && data.intents.length > 0 && data.entities[0].entity === 'amenity'
  }
}

function checkCuisine(data) {
   // console.log("intentData", data);
  if(data.entities.length > 0){
    return data.intents && data.intents.length > 0 && data.entities[0].entity === 'cuisine'
  }
}

function checkMore(data) {
   // console.log("intentData", data);
  if(data.entities.length > 0){
    return data.intents && data.intents.length > 0 && data.entities[0].entity === 'yes'
  }
}

function checkNoMore(data) {
   // console.log("intentData", data);
  if(data.entities.length > 0){
    return data.intents && data.intents.length > 0 && data.entities[0].entity === 'no'
  }
}

function checkRankBy(data) {
   // console.log("intentData", data);
  if(data.entities.length > 0){
    return data.intents && data.intents.length > 0 && data.entities[0].entity === 'rankby'
  }
}

// Set optional push notification options (such as TTL) 
var options = {
    // Set the notification to expire if not delivered within 30 seconds 
    time_to_live: 86400
};


var topicsRef = admin.database().ref('topics/conversations');
 
// Retrieve new posts as they are added to our database
topicsRef.on("child_added", function(snapshot, prevChildKey) {
  
  var topicKey = snapshot.key;  
  var topicRef = admin.database().ref('topics/conversations/' + topicKey);
  topicRef.on("child_added",function(snapshot, prevChildKey) {
 
   if(snapshot.key == "messages"){
        var messageCount = 0 ;     
        var messagesRef = topicRef.child('messages');
        messagesRef.on("child_added",function(snapshot, prevChildKey){
            var data = snapshot.val();

            sendMessageNotifications(data);
            messageCount ++ ;   
            //console.log(messageCount);
            if(messageCount > 30) {
                console.log("The message count is more than 30");
                messagesRef.once("value", function(snapshot) {
                //The callback function will only get called once since we return true
                snapshot.forEach(function(childSnapshot) {
                    console.log("The first key is " + childSnapshot.key);
                    messagesRef.child(childSnapshot.key).remove();
                    messageCount --;
                    return true;
                    });
                });
            }
            //console.log(snapshot.key);
        });
    }

  });

});


function sendPush(data, registrationIds){
    // console.log("Sending Push");
    pushyAPI.sendPushNotification(data, registrationIds, options, function (err, id) {
        // Log errors to console 
        if (err) {
            return console.log('Fatal Error', err);
        }
    
        // Log success 
        console.log('Push sent successfully! (ID: ' + id + ')');
    });
}

function processData(data) {
    switch (data.type_task) {

        case 'deleteChat':
            deleteChat(data);
            break;

        case 'add_followers_count' :
            addFollowCount(data);
            break;

        case 'decrease_followers_count' :
            decreaseFollowCount(data);
            break;

        case 'deactivate_old_device' :
             deactivate(data);

        // case 'newMsg' :
        //      var lastMsgBody = "";
        //      //sendNewMessage(data);
        //      break;

    }

}


function sendMessageNotifications(data){

    var diff = Date.now() - data.time;
    // console.log("UnixTimestamp diff: "+Date.now()+" diff: "+diff);

    // console.log("msgtime: "+ data.time);
    // console.log(data.roomId + data.id + data.name + data.text);
    if(data.roomId!=null && diff < 10000){
        var subsribeRef = admin.database().ref('topics/subscribers/' + data.roomId);
        if (subsribeRef!=null){
            subsribeRef.on("child_added",function(snapshot,prevChildKey){
                var registrationIds = [];
                // console.log(snapshot.val().pushyId);
                registrationIds.push(snapshot.val().pushyId);
                var message = {"message":{"action":"groupChat","userid":data.id,"text":data.text,"name":data.name,"roomId":data.roomId}};
                    sendPush(message,registrationIds);
            });
        }
    }
}


function addFollowCount(data) {
    var registrationIds= [];
    console.log(data.reciever_id);
    registrationIds.push(data.reciever_id);
    var data={"message":{"action":"addCount","senderId":data.sender_id}};
    sendPush(data, registrationIds);    
}

function decreaseFollowCount(data) {
    var registrationIds= [];
    console.log(data.reciever_id);
    registrationIds.push(data.reciever_id);
    var data={"message":{"action":"decreaseCount","senderId":data.sender_id}};
    sendPush(data, registrationIds);    
}


function deleteChat(data) {
    var registrationIds= [];
    console.log("deleteChat", data.reciever_id);
    registrationIds.push(data.reciever_id);
    var data={"message":{"action":"deleteChat","senderId":data.sender_id}};
    sendPush(data, registrationIds);    
}


// function sendNewMessage(data){
//     var newMessagePath = admin.database().ref('messages/'+data.path);

//     newMessagePath.once("value", function(snapshot, prevChildKey){
//         // console.log("newMessageKey", snapshot.key);

//         var newMessageKey = snapshot.key;
//         var data = snapshot.val();
//         //console.log("newMessageKeyChange", newMessageKey);
//         // console.log("msgBody", data);    
//         var count = 0;
        
//         for(var event in data){
//             var dataCopy = data[event]
//             for(key in dataCopy){
//                 if(key == "name" ){
//                  // needs more specific method to manipulate date to your needs
//                     fromUser = dataCopy[key];
//                 }

//             if(key == "text" ){
//                  // needs more specific method to manipulate date to your needs
//                     msgBody = dataCopy[key];
//                     lastMsgBody = msgBody;
//                 }

//             }
//         }

//         for(var unread in data){
//             var dataCopy = data[unread]
//             for(key in dataCopy){
//                 //console.log("keyCopy", dataCopy[key]);
//                 if(dataCopy[key] == fromUser){
//                     readvalue = dataCopy["read"];
//                     if(!readvalue)
//                         count ++;
//                 }
//             }
//         }   
//         console.log("lastmsgSame "+ lastMsgBody == msgBody); 
        
//         //console.log("fromUser", fromUser);    
//         //console.log("key", readvalue);
//         //console.log("count", count);
        
//         var users = newMessageKey.split("_");
//         for(i = 0; i < users.length; i++){
//             var userId = users[i];
//             // console.log("userId", userId);
//             // console.log("index", userId.indexOf(fromUser));
//             var registrationIds= [];
//             if(userId.indexOf(fromUser) == -1 && !readvalue){

//                 var pushyRef = admin.database().ref('users').child(userId);
//                 //console.log("pushyRef", pushyRef.);
//                 pushyRef.on("child_added", function(snapshot, prevChildKey){
                
//                 if(snapshot.key.indexOf('pushyId') >= 0){
//                     // console.log("child_added", snapshot.key);
//                     // console.log("pushy_val", snapshot.val());
                
//                     registrationIds.push(snapshot.val());
//                     var data={"message":{"action":"newMessage", "senderIds":newMessageKey, "userId": fromUser, "unread": count, "body": msgBody }};
//                     sendPush(data, registrationIds);
//                 }
                        

//             });

//             }
        
//             //console.log(users[i]);
//         }

//     });
// }


function deactivate(data) {
    
    var fcmTokenRef = admin.database().ref('users/'+data.userid+'/fcmToken');

    //console.log("fcmTokenRefPath: "+fcmTokenRef);
 
    // Retrieve new posts as they are added to our database
    fcmTokenRef.on("value", function(snapshot, prevChildKey) {

        //if(snapshot.val() != null){
            // This registration token comes from the client FCM SDKs.
            var registrationToken = snapshot.val();

            console.log("fcmToken", registrationToken);

            // See the "Defining the message payload" section below for details
            // on how to define a message payload.
            var payload = {
                data: {
                    deactivate: "true"
                }
            };

            // Send a message to the device corresponding to the provided
            // registration token.
            admin.messaging().sendToDevice(registrationToken, payload)
            .then(function(response) {
            // See the MessagingDevicesResponse reference documentation for
            // the contents of response.
                console.log("Successfully sent message:", response);
            })
            .catch(function(error) {
                console.log("Error sending message:", error);
            });
        //}
    });

    fcmTokenRef.off("value");

}


io.on('connection',function(socket){
        
    //For group chat
    socket.on('joinGroup',function(data){
    socket.join(data.roomId); 
    var room = io.sockets.adapter.rooms[data.roomId];
    if(room != null)
        var roomlength = room.length;
    
    else
        var roomlength = 0;
        console.log(roomlength);
        console.log(socket.id + "has joined  group chat with room id" + data.roomId);
        io.sockets.in(data.roomId).emit('join_group',{"nick":data.nick,"userid":data.userid}) ; 
        var ref = admin.database().ref('topics/activeNo/' + data.roomId + '/roomsize');
        ref.set(roomlength);
    });  

    //For group chat
    socket.on('leaveGroup',function(data){
        console.log(socket.id + "has left group chat with room id" + data.roomId);
        socket.leave(data.roomId); 
        var room = io.sockets.adapter.rooms[data.roomId];
        if(room != null)
            var roomlength = room.length;
        else
            var roomlength = 0;
            console.log(roomlength);
            io.sockets.in(data.roomId).emit('leave_group',{"nick":data.nick,"userid":data.userid}) ; 
            var ref = admin.database().ref('topics/activeNo/' + data.roomId + '/roomsize');
            ref.set(roomlength);
    });  
    
    socket.on('disconnect',function(){
        console.log("The socket disconnected is " + socket.id);
    });

    socket.on('blockPerson',function(data){
        var registrationIds= [];
        console.log(data.recieverId);
        registrationIds.push(data.recieverId);
        var data={"message":{"action":"blockPerson","senderId":data.senderId,"recieverId":data.recieverId}};
        sendPush(data, registrationIds);
    });
   
    socket.on('sendRefFollow',function(data){
        var registrationIds= [];
        console.log(data.recieverId + data.userId);
        registrationIds.push(data.recieverId);


        var data={"message":{"action":"reqRefFollow", "recieverId":data.recieverId, "userId":data.userId}};
    
        sendPush(data, registrationIds);
    });
  
    socket.on('startTyping',function(data){
            io.sockets.in(data.recieverId).emit('start_typing',{msg:data.message,"typingFlag":true});
    });

    socket.on('stoptyping',function(data){
            io.sockets.in(data.recieverId).emit('stop_typing',{msg:data.message,"typingFlag":false}) ;    
    });
    
    socket.on('startRoomTyping',function(data){
            io.sockets.in(data.topicid).emit('start_room_typing',{"userid":data.userid,"nick":data.nick});
    });

    socket.on('stopRoomtyping',function(data){
            io.sockets.in(data.topicid).emit('stop_room_typing',{"userid":data.userid}) ;    
    });

    socket.on('leave',function(data){
        console.log("To " + data.recieverId + ": " + data.message + " from" + data.senderId + " with SpecialFlag " + data.specialFlag + " with leftFlag " + data.leftFlag)  ;
        io.sockets.in(data.recieverId).emit('new_message',{msg:data.message,"senderId":data.senderId,"specialFlag":data.specialFlag,"leftFlag":data.leftFlag}) ;
    });
        
});


//app.get('/', routes.index);

// function createResponseData(id, name, value, attachments) {

//     var responseData = {
//         id: id,
//         name: sanitizeInput(name),
//         value: sanitizeInput(value),
//         attachements: []
//     };


//     attachments.forEach(function(item, index) {
//         var attachmentData = {
//             content_type: item.type,
//             key: item.key,
//             url: '/api/favorites/attach?id=' + id + '&key=' + item.key
//         };
//         responseData.attachements.push(attachmentData);

//     });
//     return responseData;
// }

// function sanitizeInput(str) {
//     return String(str).replace(/&(?!amp;|lt;|gt;)/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
// }

// var saveDocument = function(id, name, value, response) {

//     if (id === undefined) {
//         // Generated random id
//         id = '';
//     }

//     db.insert({
//         name: name,
//         value: value
//     }, id, function(err, doc) {
//         if (err) {
//             console.log(err);
//             response.sendStatus(500);
//         } else
//             response.sendStatus(200);
//         response.end();
//     });

// }

// app.get('/api/favorites/attach', function(request, response) {
//     var doc = request.query.id;
//     var key = request.query.key;

//     db.attachment.get(doc, key, function(err, body) {
//         if (err) {
//             response.status(500);
//             response.setHeader('Content-Type', 'text/plain');
//             response.write('Error: ' + err);
//             response.end();
//             return;
//         }

//         response.status(200);
//         response.setHeader("Content-Disposition", 'inline; filename="' + key + '"');
//         response.write(body);
//         response.end();
//         return;
//     });
// });

// app.post('/api/favorites/attach', multipartMiddleware, function(request, response) {

//     console.log("Upload File Invoked..");
//     console.log('Request: ' + JSON.stringify(request.headers));

//     var id;

//     db.get(request.query.id, function(err, existingdoc) {

//         var isExistingDoc = false;
//         if (!existingdoc) {
//             id = '-1';
//         } else {
//             id = existingdoc.id;
//             isExistingDoc = true;
//         }

//         var name = sanitizeInput(request.query.name);
//         var value = sanitizeInput(request.query.value);

//         var file = request.files.file;
//         var newPath = './public/uploads/' + file.name;

//         var insertAttachment = function(file, id, rev, name, value, response) {

//             fs.readFile(file.path, function(err, data) {
//                 if (!err) {

//                     if (file) {

//                         db.attachment.insert(id, file.name, data, file.type, {
//                             rev: rev
//                         }, function(err, document) {
//                             if (!err) {
//                                 console.log('Attachment saved successfully.. ');

//                                 db.get(document.id, function(err, doc) {
//                                     console.log('Attachements from server --> ' + JSON.stringify(doc._attachments));

//                                     var attachements = [];
//                                     var attachData;
//                                     for (var attachment in doc._attachments) {
//                                         if (attachment == value) {
//                                             attachData = {
//                                                 "key": attachment,
//                                                 "type": file.type
//                                             };
//                                         } else {
//                                             attachData = {
//                                                 "key": attachment,
//                                                 "type": doc._attachments[attachment]['content_type']
//                                             };
//                                         }
//                                         attachements.push(attachData);
//                                     }
//                                     var responseData = createResponseData(
//                                         id,
//                                         name,
//                                         value,
//                                         attachements);
//                                     console.log('Response after attachment: \n' + JSON.stringify(responseData));
//                                     response.write(JSON.stringify(responseData));
//                                     response.end();
//                                     return;
//                                 });
//                             } else {
//                                 console.log(err);
//                             }
//                         });
//                     }
//                 }
//             });
//         }

//         if (!isExistingDoc) {
//             existingdoc = {
//                 name: name,
//                 value: value,
//                 create_date: new Date()
//             };

//             // save doc
//             db.insert({
//                 name: name,
//                 value: value
//             }, '', function(err, doc) {
//                 if (err) {
//                     console.log(err);
//                 } else {

//                     existingdoc = doc;
//                     console.log("New doc created ..");
//                     console.log(existingdoc);
//                     insertAttachment(file, existingdoc.id, existingdoc.rev, name, value, response);

//                 }
//             });

//         } else {
//             console.log('Adding attachment to existing doc.');
//             console.log(existingdoc);
//             insertAttachment(file, existingdoc._id, existingdoc._rev, name, value, response);
//         }

//     });

// });

// app.post('/api/favorites', function(request, response) {

//     console.log("Create Invoked..");
//     console.log("Name: " + request.body.name);
//     console.log("Value: " + request.body.value);

//     // var id = request.body.id;
//     var name = sanitizeInput(request.body.name);
//     var value = sanitizeInput(request.body.value);

//     saveDocument(null, name, value, response);

// });


//Watson Conversation Server Side

// Endpoint to be call from the client side
app.post( '/api/message', function(req, res) {
  var workspace = process.env.WORKSPACE_ID || '<workspace-id>';
  console.log("WorkspaceID", workspace);

  if ( !workspace || workspace === '<workspace-id>' ) {
    return res.json( {
      'output': {
        'text': 'The app has not been configured with a <b>WORKSPACE_ID</b> environment variable. Please refer to the ' +
        '<a href="https://github.com/watson-developer-cloud/conversation-simple">README</a> documentation on how to set this variable. <br>' +
        'Once a workspace has been defined the intents may be imported from ' +
        '<a href="https://github.com/watson-developer-cloud/conversation-simple/blob/master/training/car_workspace.json">here</a> in order to get a working application.'
      }
    } );
  }
  var payload = {
    workspace_id: workspace,
    context: {},
    input: {}
  };

  if(req.body) {
    if ( req.body.input ) {
      payload.input = JSON.parse(req.body.input);
    }
    if ( req.body.context ) {
      // The client must maintain context/state
      payload.context = JSON.parse(req.body.context);
    }
  }


  // Send the input to the conversation service
  conversation.message( payload, function(err, data) {
    //console.log("payLoad", payload);
    if ( err ) {
       // console.log("error dude", err);
      return res.status( err.code || 500 ).json( err );
    }
    updateMessage( res, payload, data );
  });

});


// app.delete('/api/favorites', function(request, response) {

//     console.log("Delete Invoked..");
//     var id = request.query.id;
//     // var rev = request.query.rev; // Rev can be fetched from request. if
//     // needed, send the rev from client
//     console.log("Removing document of ID: " + id);
//     console.log('Request Query: ' + JSON.stringify(request.query));

//     db.get(id, {
//         revs_info: true
//     }, function(err, doc) {
//         if (!err) {
//             db.destroy(doc._id, doc._rev, function(err, res) {
//                 // Handle response
//                 if (err) {
//                     console.log(err);
//                     response.sendStatus(500);
//                 } else {
//                     response.sendStatus(200);
//                 }
//             });
//         }
//     });

// });

// app.put('/api/favorites', function(request, response) {

//     console.log("Update Invoked..");

//     var id = request.body.id;
//     var name = sanitizeInput(request.body.name);
//     var value = sanitizeInput(request.body.value);

//     console.log("ID: " + id);

//     db.get(id, {
//         revs_info: true
//     }, function(err, doc) {
//         if (!err) {
//             console.log(doc);
//             doc.name = name;
//             doc.value = value;
//             db.insert(doc, doc.id, function(err, doc) {
//                 if (err) {
//                     console.log('Error inserting data\n' + err);
//                     return 500;
//                 }
//                 return 200;
//             });
//         }
//     });
// });

// app.get('/api/favorites', function(request, response) {

//     console.log("Get method invoked.. ")

//     db = cloudant.use(dbCredentials.dbName);
//     var docList = [];
//     var i = 0;
//     db.list(function(err, body) {
//         if (!err) {
//             var len = body.rows.length;
//             console.log('total # of docs -> ' + len);
//             if (len == 0) {
//                 // push sample data
//                 // save doc
//                 var docName = 'sample_doc';
//                 var docDesc = 'A sample Document';
//                 db.insert({
//                     name: docName,
//                     value: 'A sample Document'
//                 }, '', function(err, doc) {
//                     if (err) {
//                         console.log(err);
//                     } else {

//                         console.log('Document : ' + JSON.stringify(doc));
//                         var responseData = createResponseData(
//                             doc.id,
//                             docName,
//                             docDesc, []);
//                         docList.push(responseData);
//                         response.write(JSON.stringify(docList));
//                         console.log(JSON.stringify(docList));
//                         console.log('ending response...');
//                         response.end();
//                     }
//                 });
//             } else {

//                 body.rows.forEach(function(document) {

//                     db.get(document.id, {
//                         revs_info: true
//                     }, function(err, doc) {
//                         if (!err) {
//                             if (doc['_attachments']) {

//                                 var attachments = [];
//                                 for (var attribute in doc['_attachments']) {

//                                     if (doc['_attachments'][attribute] && doc['_attachments'][attribute]['content_type']) {
//                                         attachments.push({
//                                             "key": attribute,
//                                             "type": doc['_attachments'][attribute]['content_type']
//                                         });
//                                     }
//                                     console.log(attribute + ": " + JSON.stringify(doc['_attachments'][attribute]));
//                                 }
//                                 var responseData = createResponseData(
//                                     doc._id,
//                                     doc.name,
//                                     doc.value,
//                                     attachments);

//                             } else {
//                                 var responseData = createResponseData(
//                                     doc._id,
//                                     doc.name,
//                                     doc.value, []);
//                             }

//                             docList.push(responseData);
//                             i++;
//                             if (i >= len) {
//                                 response.write(JSON.stringify(docList));
//                                 console.log('ending response...');
//                                 response.end();
//                             }
//                         } else {
//                             console.log(err);
//                         }
//                     });

//                 });
//             }

//         } else {
//             console.log(err);
//         }
//     });

// });


// http.createServer(app).listen(app.get('port'), '0.0.0.0', function() {
//     console.log('Express server listening on port ' + app.get('port'));
// });


var port = process.env.PORT || process.env.VCAP_APP_PORT || 8080;

http.listen(port, function() {
    console.log("To view your app, open this link in your browser: http://localhost:" + port);
});